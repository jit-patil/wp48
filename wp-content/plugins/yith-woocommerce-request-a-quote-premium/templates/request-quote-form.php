<?php
/**
 * Form to Request a quote
 *
 * @package YITH Woocommerce Request A Quote
 * @since   1.0.0
 * @version 1.0.0
 * @author  Yithemes
 */
$current_user = array();
if ( is_user_logged_in() ) {
    $current_user = get_user_by( 'id', get_current_user_id() );
}

$user_name = ( ! empty( $current_user ) ) ?  $current_user->display_name : '';
$user_email = ( ! empty( $current_user ) ) ?  $current_user->user_email : '';

$optional_form_text_field            = ( get_option( 'ywraq_additional_text_field' ) == 'yes' ) ? true : false;
$optional_form_text_field_required   = ( get_option( 'ywraq_additional_text_field_required' ) == 'yes' ) ? 'required' : '';
$optional_form_text_field_2          = ( get_option( 'ywraq_additional_text_field_2' ) == 'yes' ) ? true : false;
$optional_form_text_field_required_2 = ( get_option( 'ywraq_additional_text_field_required_2' ) == 'yes' ) ? 'required' : '';
$optional_form_text_field_3          = ( get_option( 'ywraq_additional_text_field_3' ) == 'yes' ) ? true : false;
$optional_form_text_field_required_3 = ( get_option( 'ywraq_additional_text_field_required_3' ) == 'yes' ) ? 'required' : '';
$optional_form_upload_field          = ( get_option( 'ywraq_additional_upload_field' ) == 'yes' ) ? true : false;
$force_user_to_register              = ( get_option( 'ywraq_force_user_to_register' ) == 'yes' ) ? 'required' : '';
?>
<div class="yith-ywraq-mail-form-wrapper">
<div class="blog_profile"><?php get_the_content(); ?></div>

    <h3><?php _e( 'Get A Quote Form', 'yith-woocommerce-request-a-quote' ) ?></h3>
    <form id="yith-ywraq-mail-form" name="yith-ywraq-mail-form" action="<?php echo esc_url( YITH_Request_Quote()->get_raq_page_url() ) ?>" method="post" enctype="multipart/form-data">

        <p class="form-row form-row-wide validate-required" id="rqa_name_row">
            <label for="rqa-name" class=""><?php _e( 'First Name:', 'yith-woocommerce-request-a-quote' ) ?>
                <abbr class="required" title="required">*</abbr></label>
            <input type="text" class="input-text " name="rqa_name" id="rqa-name" placeholder="" value="<?php echo $_POST['rqa_name']; ?>" required>
        </p>
			<p class="form-row form-row-wide validate-required" id="rqa_lastgname_row">
             <label for="rqa-lastname" class=""><?php _e( 'Last Name:', 'yith-woocommerce-request-a-quote' ) ?>
             <abbr class="required" title="required">*</abbr></label>
             <input type="text" class="input-text " name="rqa_lastname" id="rqa-lastname" placeholder="" value="<?php echo $_POST['rqa_lastname']; ?>" required>
        </p>
		
		<p class="form-row form-row-wide validate-required" id="rqa_company_row">
		 <label for="rqa-companyname" class=""><?php _e( 'Company:', 'yith-woocommerce-request-a-quote' ) ?>
		 <abbr class="required" title="required">*</abbr></label>
		 <input type="text" class="input-text " name="rqa_company" id="rqa-company" placeholder="" value="<?php echo $_POST['rqa_company']; ?>" required>
	   </p>
		<p class="form-row form-row-wide validate-required" id="rqa_address_row">
             <label for="rqa-address" class=""><?php _e( 'Address:', 'yith-woocommerce-request-a-quote' ) ?>
             <abbr class="required" title="required">*</abbr></label>
             <input type="text" class="input-text " name="rqa_address" id="rqa-address" placeholder="" value="<?php echo $_POST['rqa_address']; ?>" required>
        </p>
		

		
		<?php
    global $wpdb;
    $query = $wpdb->get_results ("SELECT * FROM " . $wpdb->prefix . "countries");
	
	echo '<p class="form-row form-row-wide validate-required" id="rqa_state_row">';
	echo '<label for="rqa-country" class="">Country:<abbr class="required" title="required">*</abbr></label>';
    echo '<select id="country" name="rqa-country"  class="contentsm">';
	 
    echo '<option> Select Country </option>';
    foreach ( $query as $result )
    {
     
       echo '<option value="'.$result->id.'">'.$result->name.'</option>';

    }        
    echo '</select>';
    echo '</p>';

  
?>


<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.9.0.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    
	$('#country').on('change', function() {
    
        var countryID = this.value;
        //alert(countryID);
        if(countryID){
			
            $.ajax({
                type:'POST',
			    url: "<?php echo plugin_dir_url( __FILE__ ) . 'merge.php'; ?>",
                data:{'country_id':countryID},
                success:function(data){
                	//console.log(data);
                    $('#state').html(data);
					$('#rqa_city').html('<option value="">Select state first</option>'); 
                }
            }); 
		}else{
            $('#state').html('<option value="">Select country first</option>');
            $('#rqa_city').html('<option value="">Select state first</option>'); 
        }
    });
	
	$('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url: "<?php echo plugin_dir_url( __FILE__ ) . 'merge.php'; ?>",
                data:'state_id='+stateID,
                success:function(data){
                    $('#rqa_city').html(data);
                }
            }); 
        }else{
            $('#rqa_city').html('<option value="">Select state first</option>'); 
        }
    });
});

</script>

		<p class="form-row form-row-wide validate-required" id="rqa_state_row">
            <label for="rqa-state" class=""><?php _e( 'State', 'yith-woocommerce-request-a-quote' ) ?></label>
		<select name="rqa_state" id="state">
 
    <option value="">Select State</option>
</select>
		</p>
			<p class="form-row form-row-wide validate-required" id="rqa_province_row">
             <label for="rqa-province" class="">
			 <?php _e( ' or Province:', 'yith-woocommerce-request-a-quote' ) ?>
            </label>
             <input type="text" class="input-text " name="rqa_province" id="rqa-province" placeholder="" value="<?php echo $_POST['rqa_province']; ?>" required>
        </p>
        
        
		<p class="form-row form-row-wide validate-required" id="rqa_city_row">
             <label for="rqa-city" class=""><?php _e( 'City:', 'yith-woocommerce-request-a-quote' ) ?>
             <abbr class="required" title="required">*</abbr></label>
             <!--<input type="text" class="input-text " name="rqa_city" id="rqa-city" placeholder="" value="<?php echo $_POST['rqa_city']; ?>" required>-->
             <select name="rqa_city" id="rqa_city">
                <option value="">Select City</option>
             </select>
        </p>
        
        
		<p class="form-row form-row-wide validate-required" id="rqa_zip_row">
             <label for="rqa-zip" class=""><?php _e( ' Zip:', 'yith-woocommerce-request-a-quote' ) ?>
			 </label>
            
             <input type="text" class="input-text " name="rqa_zip" id="rqa-zip" placeholder="" value="<?php echo $_POST['rqa_zip']; ?>" required>
        </p>
		
		
	
		
		<p class="form-row form-row-wide validate-required" id="rqa_phone_row">
             <label for="rqa-phone" class=""><?php _e( 'Phone:', 'yith-woocommerce-request-a-quote' ) ?>
               <abbr class="required" title="required">*</abbr></label>
             <input type="text" class="input-text " name="rqa_phone" id="rqa-zip" placeholder="" value="<?php echo $_POST['rqa_phone']; ?>" required>
        </p>
		
        <p class="form-row form-row-wide validate-required" id="rqa_email_row">
            <label for="rqa-email" class=""><?php _e( 'Email', 'yith-woocommerce-request-a-quote' ) ?>
                <abbr class="required" title="required">*</abbr></label>
            <input type="email" class="input-text " name="rqa_email" id="rqa-email" placeholder="" value="<?php echo $_POST['rqa_email']; ?>" required>
        </p>
		
		  <p class="form-row" id="rqa_message_row">
            <label for="rqa-message" class=""><?php _e( 'Comment:', 'yith-woocommerce-request-a-quote' ) ?><abbr class="required" title="required">*</abbr></label>
            <textarea name="rqa_message" class="input-text " id="rqa-message" placeholder="<?php _e( 'Notes on your request...', 'yith-woocommerce-request-a-quote' ) ?>" rows="5" cols="5" required></textarea>
        </p>

        <?php if( $optional_form_text_field ): ?>
            <p class="form-row form-row-wide validate-required" id="rqa_text_field_row">
                <label for="rqa_text_field_row" class=""><?php echo get_option('ywraq_additional_text_field_label') ?>
                    <?php if ( $optional_form_text_field_required == 'required' ) : ?>
                       <abbr class="required" title="required">*</abbr></label>
                    <?php endif ?>
                <input type="text" class="input-text " name="rqa_text_field" id="rqa-text-field" placeholder="" value="" <?php echo $optional_form_text_field_required ?>>
            </p>
        <?php endif ?>

        <?php if( $optional_form_text_field_2 ): ?>
            <p class="form-row form-row-wide validate-required" id="rqa_text_field_row_2">
                <label for="rqa_text_field_row_2"><?php echo get_option('ywraq_additional_text_field_label_2') ?>
                    <?php if ( $optional_form_text_field_required_2 == 'required' ) : ?>
                        <abbr class="required" title="required">*</abbr></label>
                    <?php endif ?>
                <input type="text" class="input-text " name="rqa_text_field_2" id="rqa_text_field_row_2" placeholder="" value="" <?php echo $optional_form_text_field_required_2 ?>>
            </p>
        <?php endif ?>
        <?php if( $optional_form_text_field_3 ): ?>
            <p class="form-row form-row-wide validate-required" id="rqa_text_field_row_3">
                <label for="rqa_text_field_row_3"><?php echo get_option('ywraq_additional_text_field_label_3') ?>
                    <?php if ( $optional_form_text_field_required_3 == 'required' ) : ?>
                        <abbr class="required" title="required">*</abbr></label>
                    <?php endif ?>
                <input type="text" class="input-text " name="rqa_text_field_3" id="rqa_text_field_row_3" placeholder="" value="" <?php echo $optional_form_text_field_required_3 ?>>
            </p>
        <?php endif ?>

        <?php if( $optional_form_upload_field ): ?>
            <p class="form-row form-row-wide" id="rqa_upload_field_row">
                <label for="rqa-upload-field" class=""><?php echo get_option('ywraq_additional_upload_field_label') ?>
                <input type="file" class="input-text input-upload" name="rqa_upload_field" id="rqa-upload-field">
            </p>
        <?php endif ?>
      
        <?php if( ! is_user_logged_in() && get_option('ywraq_add_user_registration_check') == 'yes' ): ?>
            <input class="input-checkbox" id="createaccount" type="checkbox" name="createaccount" value="1" <?php echo $force_user_to_register ?>/> <label for="createaccount" class="checkbox"><?php _e( 'Create an account?', 'yith-woocommerce-request-a-quote' ); ?></label>
        <?php endif ?>
        <p class="form-row">
            <input type="hidden" id="raq-mail-wpnonce" name="raq_mail_wpnonce" value="<?php echo wp_create_nonce( 'send-request-quote' ) ?>">
            <input class="button raq-send-request" type="submit" value="<?php _e( 'Get Quote', 'yith-woocommerce-request-a-quote' ) ?>">
        </p>
        <?php if ( defined( 'ICL_LANGUAGE_CODE' ) ): ?>
            <input type="hidden" class="lang_param" name="lang" value="<?php echo( ICL_LANGUAGE_CODE ); ?>" />
        <?php endif ?>

    </form>
</div>